Feature: Testing of Deckofcardapi
  Several tests are checking some deckofcardsapi.com functionality

  Scenario Outline: Draw <num1> cards from the deck and check remaining amount
    Given I have a new shuffled deck
    When I draw <num1> cards from the deck
    Then remaining cards amount in the deck is <num2>

    Examples:
      | num1 | num2 |
      | -1 | 1 |
      | 0 | 52 |
      | 27 | 25 |
      | 52 | 0 |
      | 53 | 0 |

  Scenario Outline: Create custom deck with Aces only
    Given I create deck with card codes: <card_codes>
    When I draw 4 cards from the deck
    Then all pulled cards codes are in <card_codes> set
    And remaining cards amount in the deck is 0

    Examples:
      | card_codes  |
      | AS,AD,AC,AH |

  Scenario: Draw  cards from the deck bottom
    Given I have a new shuffled deck
    When I draw card from the deck bottom
    Then response result code is 404

  Scenario Outline: Draw specific cards from the pile
    Given I have a new shuffled deck
    When I put the whole deck to the pile MyPile
    And I draw <card_codes> cards from the pile MyPile
    Then <card_codes> cards codes are absent in the pile MyPile
    And there are 47 remaining cards in MyPile

    Examples:
      | card_codes     |
      | AS,7D,2C,KH,KC |


