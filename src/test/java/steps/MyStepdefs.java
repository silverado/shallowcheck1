package steps;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import shallowcheck.pojo.Card;
import shallowcheck.pojo.Deck;
import shallowcheck.response.DrawnCardResponse;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class MyStepdefs {
    private static final String PILE_CARD_CODES_PATH = "piles.%s.cards.code";
    private static final String PILE_REMAINING_PATH = "piles.%s.remaining";

    private Properties prop;

    private Deck deck;
    private DrawnCardResponse drawnCardResponse;
    private Response response;


    @Before
    public void init() throws IOException {
        RestAssured.baseURI = "https://deckofcardsapi.com/api/deck/";
        prop = new Properties();
        prop.load(MyStepdefs.class.getClassLoader().getResourceAsStream("uri.properties"));
    }

    @Given("I have a new shuffled deck")
    public void getNewDeck() {
        deck = serializeResponse(prop.getProperty("newDeckURL"), Deck.class);
    }

    @Given("I create deck with card codes: {}")
    public void createCustomDeck(String cardSetString) {
        deck = serializeResponse(String.format(prop.getProperty("partialDeckURL"), cardSetString), Deck.class);
    }

    @When("I draw {int} cards from the deck")
    public void drawCardsFromDeck(int cards) {
        drawnCardResponse = serializeResponse(String.format(prop.getProperty("drawCardsURL"), deck.getDeckId(), cards), DrawnCardResponse.class);
    }

    @When("I draw {} cards from the pile {}")
    public void drawCardsFromPile(String cards, String pileName) {
        String url = String.format(prop.getProperty("drawParticularCardCodesFromPileURL"), deck.getDeckId(), pileName, cards);
        response = given().log().all().when().get(url)
                .then().log().body()
                .and()
                .assertThat().statusCode(200).extract().response();
    }

    @When("I put the whole deck to the pile {}")
    public void putWholeDeckToPile(String pileName) {
        String url = String.format(prop.getProperty("drawCardsURL"), deck.getDeckId(), 52);
        List<String> codesList = given().when().get(url)
                .then().log().body()
                .and()
                .assertThat().statusCode(200).extract().path("cards.code");
        url = String.format(prop.getProperty("addCardsToPileURL"), deck.getDeckId(), pileName, StringUtils.join(codesList, ","));
        get(url)
                .then().log().body()
                .and()
                .assertThat().statusCode(200);
    }

    @When("I draw card from the deck bottom")
    public void drawCardsFromTheBottom() {
        String url = String.format(prop.getProperty("drawBottomURL"), deck.getDeckId());
        response = when().get(url).then().extract().response();
    }

    @Then("remaining cards amount in the deck is {int}")
    public void validateRemaining(int remaining) {
        assertThat(drawnCardResponse.getRemainingCards(), Matchers.equalTo(remaining));
    }

    @Then("all pulled cards codes are in {} set")
    public void validateValues(String codeList) {
        List<String> expectedCodes = Arrays.asList(codeList.split(","));
        Collections.sort(expectedCodes);
        List<String> actualCodes = drawnCardResponse.getCards()
                .stream()
                .map(Card::getCode)
                .sorted()
                .collect(Collectors.toList());
        assertThat("Code lists are not equal", actualCodes, is(expectedCodes));
    }

    @Then("response result code is {int}")
    public void validateResponseCode(int responseCode) {
        assertThat(response.statusCode(), equalTo(responseCode));
    }

    @Then("{} cards codes are absent in the pile {}")
    public void validateCardAbsenceInPile(String cardCodes, String pileName) {
        String codePath = String.format(PILE_CARD_CODES_PATH, pileName);
        assertThat(response.getBody().path(codePath), not(hasItems(cardCodes.split(","))));
    }

    @Then("there are {int} remaining cards in {}")
    public void validatePileRemaining(int remaining, String pileName) {
        String path = String.format(prop.getProperty("listPileURL"), deck.getDeckId(), pileName);
        get(path)
                .then().log().body()
                .and()
                .assertThat().statusCode(200)
                .and().assertThat().body(String.format(PILE_REMAINING_PATH, pileName),
                equalTo(remaining));
    }

    private <T> T serializeResponse(String url, Class<T> typeRef) {
        return given().log().all().when().get(url)
                .then().log().all()
                .and()
                .assertThat().statusCode(200)
                .and().extract().as(typeRef);
    }
}
