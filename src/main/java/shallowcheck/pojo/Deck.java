package shallowcheck.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Deck {
    private boolean success;
    @JsonProperty("deck_id")
    private String deckId;
    @JsonProperty("shuffled")
    private boolean isShuffled;
    @JsonProperty("remaining")
    private int remainingCards;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getDeckId() {
        return deckId;
    }

    public void setDeckId(String deckId) {
        this.deckId = deckId;
    }

    public boolean isShuffled() {
        return isShuffled;
    }

    public void setShuffled(boolean shuffled) {
        isShuffled = shuffled;
    }

    public int getRemainingCards() {
        return remainingCards;
    }

    public void setRemainingCards(int remainingCards) {
        this.remainingCards = remainingCards;
    }


}
