package shallowcheck.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Card {
    @JsonProperty("image")
    private String imageUrl;
    @JsonProperty("value")
    private String value;
    @JsonProperty("suit")
    private String suit;
    @JsonProperty("code")
    private String code;
    @JsonIgnore
    @JsonProperty("images")
    private Image images;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSuit() {
        return suit;
    }

    public void setSuit(String suit) {
        this.suit = suit;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getImages() {
        return images;
    }

    public void setImages(Image images) {
        this.images = images;
    }

}
