package shallowcheck.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import shallowcheck.pojo.Card;

import java.util.List;

public class DrawnCardResponse {
    private boolean success;
    @JsonProperty("deck_id")
    private String deckId;
    private List<Card> cards;
    @JsonProperty("error")
    private String error;
    @JsonProperty("remaining")
    private int remainingCards;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getDeckId() {
        return deckId;
    }

    public void setDeckId(String deckId) {
        this.deckId = deckId;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public int getRemainingCards() {
        return remainingCards;
    }

    public void setRemainingCards(int remainingCards) {
        this.remainingCards = remainingCards;
    }

}
