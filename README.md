Required software:
* Java JDK 8

How to start tests:
1) Open command line
2) go to project folder
3) execute command:
  * Windows: gradlew.bat clean runTest
  * Linux: ./gradlew clean runTest

HTML report location: target\cucumber-html-report.html

Please note:
Test case #3 was replaced with similar one due to 404 response from https://deckofcardsapi.com/api/deck/<<deck_id>>/draw/bottom/